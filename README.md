# webmapping

Visualisation des résultats obtenus dans le cadre du projet IQS-Morges,
réalisé par [HEPIA](https://www.hesge.ch/hepia/), [HEIA-FR](https://www.heia-fr.ch/) et [HEIG-VD](https://heig-vd.ch/) pour [Région-Morges](https://www.regionmorges.ch/).


Les résultats sont visibles à l'adresse suivante : [https://iqs.heig-vd.ch/maps](https://iqs.heig-vd.ch/maps)

## Installation

* Cloner le répertoire
```
git clone https://gitlab.com/heig-vd-geo/iqs/webmapping
cd webmapping
```

* Installer docker et docker compose
```
sudo snap install docker
sudo apt install docker-compose
```

* Renseigner le répertoire comportant les données de webmapping dans le fichier docker-compose.yml, ligne 11 :
"- /mnt/Data/05_Projects/01_IQS/data:/usr/src/app/map"

* Créer ou ajouter les certificats SSL dans le répertoire certif
```
openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout key.pem -out cert.pem
```

* Créer et lancer les containers docker
```
docker-compose build
docker-compose up -d
```
