from flask import Flask, send_from_directory, render_template
import os
import glob

from config import BaseConfig

app = Flask(__name__)

app.config.from_object(BaseConfig)


# TILES_DIR = "data"

def getAllMaps():
    maps = glob.glob(f"./{app.config['TILES_DIR']}/note*")
    maps = [m.replace(app.config['TILES_DIR'], "")[3:] for m in maps]
    maps.sort()
    return maps


########################################################################
# home
########################################################################
@app.route('/')
def home():
    return render_template('home.html', name="Home", maps=getAllMaps())


########################################################################
# maps
########################################################################
@app.route('/maps')
def maps():
    return render_template('maps.html', name="Maps", maps=getAllMaps())


########################################################################
# map
########################################################################
@app.route('/map/<m>')
def map(m):
    if os.path.isdir(os.path.join(app.config['TILES_DIR'], m)):
        return render_template('map.html', name="Map", map=m)


########################################################################
# Route to get local OSM tile
########################################################################
@app.route('/<m>/<z>/<x>/<y>')
def tile(m, z, x, y):
    fn = os.path.join(app.config['TILES_DIR'], m, z, x, y)
    dn = os.path.join(app.config['TILES_DIR'], m, z, x)
    if os.path.isfile(fn):
        return send_from_directory(dn, y)
    else:
        return ""  # send_from_directory("static/","missing-tile-256x256.png")


########################################################################
if __name__ == "__main__":
    # Flask.run(app, host= '0.0.0.0')
    app.run(ssl_context='adhoc')
